FROM continuumio/miniconda3:4.5.4

# Metadata
LABEL version="1.0"
LABEL author="Bhavin Chandarana"
LABEL organization="Seligent Consulting"
LABEL contact="admin@seligentconsulting.com"
LABEL description="Docker image to run CI jobs for projects in python"

ARG PIP_CACHE_DIR="/root/.cache/pip"
ENV PIP_CACHE_DIR="/root/.cache/pip"

# Install, update, upgrade
RUN apt-get update && \
    apt-get install -y xvfb=2:1.19.2-1+deb9u2 libsndfile1=1.0.27-3 && \
    apt-get clean && \
    pip install pip==18.1 && \
    pip install pylint==2.1.1 autopep8==1.4.2 && \
    pip install awscli==1.16.42 dvc==0.19.15 && \
    pip install dvc[all] && \
    rm -rf $PIP_CACHE_DIR

# Setup xvfb
ENV DISPLAY :99.0
COPY bin/xvfb.sh /etc/init.d/xvfb

# Setup aws creds
ENV AWS_ACCESS_KEY_ID="NICE_TRY"
ENV AWS_SECRET_ACCESS_KEY="NICEST_TRY"

# Setup workspace settings
ENV PYTHON_EXECUTOR="/opt/conda/bin/python"
ENV PYTHON_LINT_PYLINT=false
ENV PYTHON_LINT_PYLINT_EXECUTOR="/opt/conda/bin/pylint"
ENV PYTHON_LINT_PEP8=false
ENV PYTHON_LINT_PEP8_EXECUTOR="/opt/conda/bin/autopep8"

# Setup entrypoint
COPY bin/entrypoint.sh /entrypoint.sh
RUN chmod +x /entrypoint.sh /etc/init.d/xvfb

ENTRYPOINT [ "/entrypoint.sh" ]
